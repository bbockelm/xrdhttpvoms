Name:		xrdhttpvoms
Version:	0.2.6
Release:	2%{?dist}
Summary:	VOMS security extractor plugin for XrdHTTP
Group:		Applications/Internet
License:	ASL 2.0
URL:		https://svnweb.cern.ch/trac/lcgdm
# The source of this package was pulled from upstream's vcs. Use the
# following commands to generate the tarball:
# svn export http://svn.cern.ch/guest/lcgdm/xrdhttpvoms/tags/xrdhttpvoms_0_1_0 xrdhttpvoms-0.1.0
# tar -czvf xrdhttpvoms-0.1.0.tar.gz xrdhttpvoms-0.1.0
Source0:	%{name}-%{version}.tar.gz
Buildroot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{?fedora}%{!?fedora:0} >= 10 || %{?rhel}%{!?rhel:0} >= 6
BuildRequires:	boost-devel >= 1.41.0
%else
BuildRequires:	boost141-devel
%endif
BuildRequires:	cmake
BuildRequires:	xrootd >= 4.0.0
BuildRequires:	xrootd-devel >= 4.0.0
BuildRequires:	xrootd-server-devel >= 4.0.0
BuildRequires:	voms-devel >= 2.0.10

Requires:	xrootd >= 4.0.0
Requires:	voms >= 2.0.10


%description
This package provides the VOMS security extractor plugin for XrdHTTP. Given
a working setup of XrdHTTP, this library will be able to extract the VOMS
information from the certificate presented by the client. The extracted
information will be available for other plugins to apply authorization rules.

%prep
%setup -q -n %{name}-%{version}

%build
ls -lR /usr/include/xrootd
%cmake . -DCMAKE_INSTALL_PREFIX=/ -DCMAKE_BUILD_TYPE=RelWithDebInfo 

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}

make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root,-)
%{_libdir}/libXrdHttpVOMS-5.so*
%doc README RELEASE-NOTES
%license LICENSE

%post   -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%changelog
* Thu Jun 24 2020 Fabrizio Furano <fabrizio.furano@cern.ch> - 0.2.6-2
- Switch to xrootd 5
* Tue Mar 14 2017 Fabrizio Furano <fabrizio.furano@cern.ch> - 0.2.4-2
- First epel release

